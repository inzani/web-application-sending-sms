﻿using Microsoft.AspNetCore.Mvc;
using Model;
using Services;

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IMessageSmsService _messageSmsService;
        public ValuesController(IMessageSmsService messageSmsService) {
            _messageSmsService = messageSmsService;
        }

        [HttpPost]
        public void SendInformation(User Message)
        {
            _messageSmsService.SendMessage(Message);
        }

        [HttpGet]
        public User[] GetInformation()
        {
            return _messageSmsService.GetMessage();
        }
    }
}
