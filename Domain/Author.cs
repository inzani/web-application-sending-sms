﻿namespace Domain
{
    public class Author
    {
        public string Name { get; set; }
        public long Number { get; set; }
        public string Text { get; set; }
        public string Status { get; set; }
        public string Date { get; set; }
        
    }
}
