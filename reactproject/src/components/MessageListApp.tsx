import { useEffect, useState } from 'react'
import './MessageList.css';

function MessageList() {
    
    const url = 'https://localhost:44357/api/Values'
    const [messages, setMessage] = useState<any[] | null>();
    const [statusErr, setStatusErr] = useState<any | null>();

    useEffect(() => {
        fetch(url)
            .then((res) => res.json())
            .then((resJson) => {
                setMessage(resJson)
            })
            .catch(function (err) {
                setStatusErr(err.message)
            })
    }, [])

    if (statusErr == undefined) {
        return (
            <div className="Message">
                {messages?.map((message: any) => (
                    <div className="bodyMessage">
                        <ul><li>
                        </li><label>Имя отправителя</label><li>
                                {message.name}
                            </li><label >Текст сообщения</label><li>
                                <div>{message.message}</div>
                            </li><label>Номер</label><li>
                                {message.number}
                            </li><label>Дата отправки</label><li>
                                {message.date}
                            </li><label>Статус сообщения</label><li>
                                {message.status}
                            </li>
                        </ul>
                    </div>
                ))}
            </div>
        )
    }
    else {
        return (
                <div className="Error">
                    <p>Ошибка 500, не удалось установить соединение с сервером</p>
                </div>
            )
    }
}

export default MessageList;