import React from 'react';
import './AppOne.css';
import loginUser from '../Api/Api'

function AppOne() {
    const [name, setName] = React.useState('');
    const [number, setNumber] = React.useState('');
    const [message, setMessage] = React.useState('')

    const handleSubmit = async (e: React.FormEvent) => {
        e.preventDefault();

        setTimeout(() => {
            window.location.reload();
        }, 200);

        await loginUser({
            name,
            message,
            number
        });
    }

    return (
        <div className="AppOne" >
            <label id="FormOne" >Введите свое имя</label>
            <input id="FormOne" type="text" onChange={(e) => setName(e.target.value)} />
            <label id="FormOne" >Введите номер</label>
            <input id="FormOne" className="Number" onChange={(e) => setNumber(e.target.value)} />
            <label id="FormOne">Введите сообщение</label>
            <textarea name="comment" id="FormMessage" onChange={(e) => setMessage(e.target.value)} />
            <button id="FormOne" type="submit" onClick={handleSubmit}>Отправить</button>
        </div>
  );
}

export default AppOne;

