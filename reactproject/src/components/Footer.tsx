import React, { useEffect } from 'react';
import './Footer.css';

function Footer() {
    return (
        <footer className="contentFooter">
            <div className="Author">
                <ul><label>Ссылки на автора</label>
                    <li><a href="https://gitlab.com/inzani">
                        <img className="gitLabImage" alt="Ссылка на мой GitLab" />
                    </a>
                    <a href="https://vk.com/evilgamers">
                        <img className="vkImage" alt="Ссылка на страницу в Вк" />
                    </a>
                   <a href="https://t.me/inzani">
                        <img className="telegramImage" alt="Ссылка на мой Telegram" />
                    </a></li>
                </ul>
            </div>
        </footer>
  );
}

export default Footer;

