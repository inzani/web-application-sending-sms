async function loginUser(credentials: { name: string; message: string; number: string; }) {
    return fetch('https://localhost:44357/api/Values', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(credentials)
    })
        .then(data => data.json())
}
export default loginUser;
