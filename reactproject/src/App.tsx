import './App.css';
import AppOne from './components/AppOne';
import Footer from './components/Footer';
import MessageListApp from './components/MessageListApp';

function App() {
    return (
        <body>
            <div className = "content">
                <div className="App">
                    <AppOne />
                </div>
            
                <div className="AppTwo">
                    <MessageListApp />
                </div>
                <footer>
                    <Footer />
                </footer>
            </div>
        </body>
  );
}

export default App;

