﻿using DataBase;
using Domain;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class MessageSmsService: IMessageSmsService
    {
        private readonly IDbStorage _dbStorage;
        private readonly Random _random;

        public MessageSmsService(IDbStorage dbStorage)
        {
            _dbStorage = dbStorage;
            _random = new Random();
        }

        public User[] GetMessage() {
            var getDb = _dbStorage.Author.ToArray().ToList();

            List<User> Message = new();

            for (int i = 0; i < getDb.Count; i++)
            {
                var data = new User()
                {
                    Name = getDb[i].Name,
                    Message = getDb[i].Text,
                    Number = getDb[i].Number,
                    Status = getDb[i].Status,
                    Date = getDb[i].Date,
                };
                Message.Add(data);
            }

            User[] user = new User[Message.Count];

            for(int i = 0; i < Message.Count; i++)
            {
                user[i] = Message[i];
            }
            return user;
        }

        private string CheckRandom()
        {
            Dictionary<int, string> statusMessage = new()
            {
                { 1, "Отправлено" },
                { 2,  "Ошибка отправки"},
                { 3, "Доставлено"}
            };
            var MessageStatusCode = _random.Next(1, 4);

            if (MessageStatusCode == statusMessage.Keys.First(p => p == MessageStatusCode))
            {
                return statusMessage[MessageStatusCode];
            }
            else
                return null;
        }

        public void SendMessage(User Message)
        {
            var user = new Author
            {
                Name = Message.Name,
                Text = Message.Message,
                Number = Message.Number,
                Status = CheckRandom(),
                Date = DateTime.Now.ToString(),
            };
            _dbStorage.Author.Add(user);
            _dbStorage.Save();
        }
    }
}
