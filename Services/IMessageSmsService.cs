﻿using Model;

namespace Services
{
    public interface IMessageSmsService
    {
        User[] GetMessage();
        void SendMessage(User Message);
    }
}
