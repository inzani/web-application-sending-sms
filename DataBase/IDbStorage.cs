﻿using Microsoft.EntityFrameworkCore;
using Domain;

namespace DataBase
{
    public interface IDbStorage
    {
        DbSet<Author> Author { get; set; }
        void Save();
    }
}
