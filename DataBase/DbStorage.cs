﻿using Microsoft.EntityFrameworkCore;
using Domain;

namespace DataBase
{
    public class DbStorage: DbContext, IDbStorage
    {

        public DbSet<Author> Author { get; set; }

        public DbStorage(DbContextOptions<DbStorage> options)
            : base(options) { 
            Database.EnsureCreated(); 
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>().HasKey(p => p.Number);
            base.OnModelCreating(modelBuilder);
        }

        public void Save()
        {
            SaveChanges();
        }
    }
}