﻿using Model;

namespace Interface
{
    public interface IMessageSmsService
    {
        User[] GetMessage();
        void SendMessage(User Message);
    }
}
