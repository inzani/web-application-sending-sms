﻿namespace Model
{
    public class User
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public long Number { get; set; }
        public string Status { get; set; }
        public string Date { get; set; }
    }
}
